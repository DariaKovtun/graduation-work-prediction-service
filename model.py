from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Sequence, create_engine
from sqlalchemy.types import BIGINT, Integer, TIMESTAMP
from sqlalchemy.dialects.postgresql import ARRAY
import pandas as pd
import numpy as np
import gc

__engine = create_engine('postgresql+psycopg2://postgres:postgres@localhost:5432/bloom_db')
__Base = declarative_base()


class FilterData(__Base):
    __tablename__ = 'filter_data'
    id = Column('id', BIGINT, Sequence('filter_data_id_seq'), primary_key=True, nullable=False)
    filter = Column('filter', ARRAY(Integer), nullable=False)
    start_date = Column('start_date', TIMESTAMP, nullable=False)
    end_date = Column('end_date', TIMESTAMP, nullable=False)
    amount_of_objects = Column('amount_of_objects', Integer)
    distinctive_objects = Column('distinctive_objects', Integer)
    real_distinctive_objects = Column('real_distinctive_objects', Integer)


def create_all():
    __Base.metadata.create_all(__engine)


def load_df():
    df_gen = pd.read_sql_table('filter_data',
                               con=__engine,
                               schema='public',
                               columns=['id', 'real_distinctive_objects', 'filter'],
                               chunksize=500)
    for df in df_gen:
        print(df.columns)
        df = pd.concat([df.drop('filter', axis=1), pd.DataFrame(df['filter'].tolist(), dtype=np.int16)], axis=1)
        # df['id'] = pd.to_numeric(df['id'])
        df['real_distinctive_objects'] = pd.to_numeric(df['real_distinctive_objects'])
        yield df


def load_df_from_query(limit, offset=0):
    sql = "SELECT real_distinctive_objects, filter " \
          "FROM filter_data " \
          f"LIMIT {limit} offset {offset}"
    df = pd.read_sql(sql, con=__engine)
    filter_size = len(df['filter'].tolist()[0])
    df = pd.concat(
        [df.drop('filter', axis=1),
         pd.DataFrame(df['filter'].tolist(),
                      columns=list([f'bit_{bit}' for bit in range(0, filter_size)]),
                      dtype=np.int16)],
        axis=1)
    # df['id'] = pd.to_numeric(df['id'])
    df['real_distinctive_objects'] = pd.to_numeric(df['real_distinctive_objects'])

    return df


def load_df_gen_from_query(chunk_size, max_rows=None, skip_first=None):
    with __engine.connect() as conn:
        count = conn.execute('SELECT COUNT(*) FROM filter_data').scalar()
    if not count:
        return None
    if skip_first:
        count -= skip_first
    if max_rows:
        count = min(count, max_rows)
    print(f'There is {count} rows to be loaded')
    chunks = count // chunk_size
    if count % chunk_size:
        chunks += 1
    for i in range(0, chunks):
        print(f'Yield next chunk #{i}')
        yield load_df_from_query(chunk_size, skip_first + i * chunk_size)
