import pandas as pd

import model as m
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score, accuracy_score
from joblib import dump, load

from os.path import exists


def measure_time(action):
    def decorator(func):
        from time import time

        def wrapper(*args, **kwargs):
            start = time()
            func(*args, **kwargs)
            end = time()
            print(f'[*] {action} took {end - start} seconds.')

        return wrapper
    return decorator


def plot(predicted, actual):
    x_axis = range(len(actual))
    plt.ylabel('Distinctive objects')
    plt.plot(x_axis, actual, 'b.', alpha=0.7, label='actual')
    plt.plot(x_axis, predicted, 'r.', alpha=0.7, label='predicted')
    plt.legend(loc='best', fancybox=True, shadow=True)
    plt.grid(True)

    plt.show()


def scattered_plot(predicted, actual):
    fig, axis = plt.subplots()
    axis.scatter(predicted, actual, edgecolors=(0, 0, 1))
    min_max_actual = [min(actual), max(actual)]
    axis.plot(min_max_actual, min_max_actual, 'r--', lw=3)
    axis.set_xlabel('Predicted')
    axis.set_ylabel('Actual')


if __name__ == '__main__':
    model_filename = 'bloom_filter.mdl'
    if exists(model_filename):
        model = load(model_filename)
        sample = m.load_df_from_query(50, offset=2500)
        scaler = load('scaler.mdl')
        test_x, test_y = sample.drop('real_distinctive_objects', axis=1), sample['real_distinctive_objects']
        test_x = scaler.transform(test_x)
    else:
        print('Loading data')
        df = m.load_df_from_query(2500)
        print('Data is loaded')
        train, test = train_test_split(df, test_size=0.1, shuffle=True)
        train_x, train_y = train.drop('real_distinctive_objects', axis=1), train['real_distinctive_objects']
        test_x, test_y = test.drop('real_distinctive_objects', axis=1), test['real_distinctive_objects']
        print('Train-test dataframes are formed')
        scaler = MinMaxScaler()
        scaler.fit(train_x)
        train_x = scaler.transform(train_x)
        test_x = scaler.transform(test_x)
        print('Train-test inputs are scaled')
        model = LinearRegression()
        model.fit(train_x, train_y)
        print('Model is trained')
        dump(model, model_filename)
        print('Model is saved')

    prediction = model.predict(test_x)

    lst = zip(test_y, prediction)
    lst = sorted(lst, key=lambda e: e[0])
    test_y = list([e[0] for e in lst])
    prediction = list([e[1] for e in lst])

    plot(prediction, test_y)

    mae = mean_absolute_error(test_y, prediction)
    mse = mean_squared_error(test_y, prediction)
    r2 = r2_score(test_y, prediction)

    print(f'MAE is {mae}')
    print(f'MSE is {mse}')
    print(f'R2 is {r2}')

